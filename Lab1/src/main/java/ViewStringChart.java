import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ViewStringChart  {

    private String title;

    public ViewStringChart(String title) {
        this.title = title;
    }

    public void showChartForCollection(List<String> collection) {
        Map<Integer, Long> collect = collection.stream()
                .collect(
                        Collectors.groupingBy(
                                String::length,
                                Collectors.counting()));

        XChartMainFrame.main("",title+" Frequency ", collect,null);
    }
}
