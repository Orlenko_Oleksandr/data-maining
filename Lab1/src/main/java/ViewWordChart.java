import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ViewWordChart {
    private String title;

    public ViewWordChart(String title) {
        this.title = title;
    }




    public void showChartForCollection(List<Pair<String, Long>> collection) {
        Map<Integer, Long> collected = collection.stream()
                .collect(Collectors.groupingBy(
                        (element) -> element.getLeft().length(),
                        Collectors.summingLong(Pair::getRight))
                );

        XChartMainFrame.main("",title+" Frequency", collected,null);
    }
}
