import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Application {

    private AbstractMap<EmailType, List<String>> enumMap = new EnumMap<>(EmailType.class);
    private Pattern charactersCleaner = Pattern.compile("[-!$%^&*()_+|~=`{}\\[\\]:\";'<>?,.\\/]");

    public static void main(String[] args){
        new Application();
    }

    public Application() {
        run();
    }

    private void run() {
        List<String> fileList = new ArrayList<>();

        try {
            fileList = Files
                    .lines(Paths.get(ClassLoader.getSystemResource("sms-spam-corpus.csv").toURI()), StandardCharsets.ISO_8859_1)
                    .skip(1)
                    .map(String::toLowerCase)
                    .collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        Map<EmailType, List<String>> collect1 = fileList.stream()
                .map(this::pair)
                .collect(
                        Collectors.groupingBy(
                                Pair::getLeft,
                                Collectors.mapping(Pair::getRight, Collectors.toList())));

        collect1.forEach((k,v) -> new ViewStringChart(k.name() + " / MESSAGE").showChartForCollection(v));

        Map<EmailType, List<Pair<String, Long>>> collect = fileList.stream()
                .map(this::pair)
                .map(this::analyze)
                .collect(
                        Collectors.groupingBy(
                                Pair::getLeft,
                                Collectors.flatMapping(pair -> pair.getRight().stream(), Collectors.toList())))
                .entrySet()
                .stream()
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey,
                                Collectors.flatMapping(entr -> this.countWords(entr).stream(), Collectors.toList())
                        )
                );

       /* collect.forEach((key, value) -> {
            System.out.println(key + ":" + value);
        });*/
       collect.forEach((k,v)->{
           Collections.sort(v, new Comparator<Pair>() {
               @Override
               public int compare(Pair lhs, Pair rhs) {
                   // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                   int i =  (Long) lhs.getRight() >  (Long)rhs.getRight() ? -1 :(Long) lhs.getRight() < (Long) rhs.getRight() ? 1 : 0;

                   return i;
               }
           });
       });
       collect.forEach((k,v)->{
           XChartMainFrame.main("",k.toString(),null,v);
       });


        collect.forEach((k,v) -> new ViewWordChart(k.name() + " / WORDS").showChartForCollection(v));
            collect.forEach((key,value)->{
                try {
                    FileWriter fileWriter = new FileWriter("output/"+key+"Statistic.txt",false);
                    value.forEach((pair)->{
                        try {
                            fileWriter.write(pair.getLeft()+" : ");
                            fileWriter.write(String.valueOf(pair.getRight()));
                            fileWriter.append("\n");
                            fileWriter.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
    }

    private List<Pair<String, Long>> countWords(Map.Entry<EmailType, List<String>> entry) {
        return entry.getValue().stream()
                .collect(
                        Collectors.groupingBy(
                                Function.identity(),
                                Collectors.counting())
                )
                .entrySet()
                .stream()
                .map(entry1 -> new Pair<>(entry1.getKey(), entry1.getValue()))
                .collect(Collectors.toList());

    }

    private Pair<EmailType, String> pair(String line) {
        String[] split = line.split(",", 2); //emailType and message
        return new Pair<>(EmailType.valueOf(split[0].toUpperCase()), split[1].toLowerCase().substring(0, split[1].length() - 3));
    }

    private Pair<EmailType, List<String>> analyze(Pair<EmailType, String> pair) {
        List<String> collected = new ArrayList<>();

        try {
            collected = analyze(pair.getRight(), new StopAnalyzer(EnglishAnalyzer.getDefaultStopSet()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Pair<>(pair.getLeft(), collected);
    }

    private List<String> analyze(String text, Analyzer analyzer) throws IOException{
        List<String> result = new ArrayList<>();
        TokenStream tokenStream = analyzer.tokenStream(null, new StringReader(text));
        CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream = new PorterStemFilter(tokenStream);
        tokenStream.reset();
        while(tokenStream.incrementToken()) {
            result.add(attr.toString());
        }
        return result;
    }


}