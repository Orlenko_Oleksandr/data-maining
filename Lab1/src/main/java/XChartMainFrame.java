


import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.markers.SeriesMarkers;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;


public class XChartMainFrame extends JFrame {
    private JPanel contentPane;
    List<Integer> xData;
    List<Long> yData;
    private XYChart chart;


    public static void main(String args, String name, Map<Integer, Long> collect,List<Pair<String,Long>> collect2) {
        EventQueue.invokeLater(() -> {
            try {
                XChartMainFrame frame = new XChartMainFrame(name, collect,collect2);
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the frame.
     */
    public XChartMainFrame(String name, Map<Integer, Long> collect,List<Pair<String,Long>> collect2) {
        setResizable(true);
        setTitle(name);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 900, 750);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panelButtons = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panelButtons.getLayout();
        flowLayout.setHgap(15);
        contentPane.add(panelButtons, BorderLayout.SOUTH);

        xData = new CopyOnWriteArrayList<>();
        yData = new CopyOnWriteArrayList<>();
        JButton btnNewButtonExit = new JButton("Exit");
        btnNewButtonExit.addActionListener(e -> System.exit(0));
        panelButtons.add(btnNewButtonExit);

        JPanel panelData = new JPanel();
        contentPane.add(panelData, BorderLayout.NORTH);
        XChartPanel<XYChart> chartPanel;
        if(collect!=null) {
            collect =  collect.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(
                            Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
            xData.addAll(collect.keySet());
            yData.addAll(collect.values());
            chartPanel = createChart(name, Collections.max(collect.values()));
        }
        else
            if(collect2!=null)
         chartPanel = createChart2(collect2);
            else chartPanel = null;
        contentPane.add(chartPanel, BorderLayout.CENTER);
    }




    private XChartPanel createChart(String name,double ymax) {
        double[] xmid = new double[2];
        double[] ymid = new double[2];
        xmid[0] = (xData.get(0)+xData.get(xData.size()-1))/2.0;
        xmid[1] = (xData.get(0)+xData.get(xData.size()-1))/2.0;
        ymid[0] = 0;
        ymid[1] = ymax;
        chart = new XYChartBuilder().xAxisTitle("X").yAxisTitle("Y").build();

        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNE);

        XYSeries series = chart.addSeries(name, xData, yData);
        series.setMarker(SeriesMarkers.CIRCLE);
        series.setMarkerColor(Color.RED);
        XYSeries series1 = chart.addSeries(name+" avarage",xmid,ymid);
        series1.setMarker(SeriesMarkers.CIRCLE);
        series1.setMarkerColor(Color.GREEN);

        XChartPanel<XYChart> chartPanel = new XChartPanel<>(chart);

        return chartPanel;
    }

    private XChartPanel createChart2( List<Pair<String,Long>> collect) {
        chart = new XYChartBuilder().xAxisTitle("X").yAxisTitle("Y").build();
        int values = 0;
        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNE);
        if(collect.size()>20){
            values = 20;
        }else{
            values = collect.size();
        }

        for(int i=0;i<values;i++){
            double[] xPoint = {i+1};
            double[] yPoint = {(double) collect.get(i).getRight()};
            XYSeries xySeries = chart.addSeries(collect.get(i).getLeft().toString(), xPoint, yPoint);
            xySeries.setMarker(SeriesMarkers.CIRCLE);
        }

        XChartPanel<XYChart> chartPanel = new XChartPanel<>(chart);

        return chartPanel;
    }

}
