import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.markers.SeriesMarkers;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;


public class XChartMainFrame extends JFrame {
    private final JComboBox<String> listFiles;
    private JPanel contentPane;
    List<Double> xData;
    List<Double> yData;
    private JPanel panelData = new JPanel();
    private XYChart chart;
    private List<Color> colors = Arrays.asList(new Color[]{Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY, Color.GRAY, Color.GREEN, Color.LIGHT_GRAY, Color.MAGENTA,
            Color.ORANGE, Color.PINK,  Color.YELLOW,Color.RED});
    private List<KlastersAndPoint> klastersAndPointList;


    public static void main(String args, String name) {
        EventQueue.invokeLater(() -> {
            try {
                XChartMainFrame frame = new XChartMainFrame(name);
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void addInfoIntoArrays(List<String> fileList){

        for(String st:fileList){
            String beforesplit = st.substring(4);
            String[] split = beforesplit.split("   ",2);
            this.xData.add(Double.valueOf(split[0]));
            this.yData.add(Double.valueOf(split[1]));

        }

    }


    /**
     * Create the frame.
     */
    public XChartMainFrame(String name) {
        setResizable(true);
        setTitle(name);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 900, 750);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);


        JPanel panelButtons = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panelButtons.getLayout();
        flowLayout.setHgap(15);
        contentPane.add(panelButtons, BorderLayout.SOUTH);

        JPanel listFilespanel = new JPanel();
        FlowLayout flowLayout1 = (FlowLayout) listFilespanel.getLayout();
        flowLayout1.setHgap(15);

        JPanel sublistFilespanel1 = new JPanel();
        FlowLayout subflowLayout1 = (FlowLayout) sublistFilespanel1.getLayout();
        subflowLayout1.setHgap(15);

        JPanel sublistFilespane2 = new JPanel();
        FlowLayout subflowLayout2 = (FlowLayout) sublistFilespane2.getLayout();
        subflowLayout2.setHgap(15);



        listFiles = new JComboBox<>();
         listFiles.addItem("birch1.txt");
        listFiles.addItem("birch2.txt");
        listFiles.addItem("birch3.txt");
        listFiles.addItem("s1.txt");

        JButton listFilesbutton = new JButton("Load");
        sublistFilespanel1.add(listFiles,BorderLayout.WEST);
        sublistFilespanel1.add(listFilesbutton,BorderLayout.EAST);

        JTextArea klaster = new JTextArea(1,20);
        klaster.setEnabled(true);
        klaster.setEditable(true);

        JButton start = new JButton("start");

        sublistFilespane2.add(klaster,BorderLayout.WEST);
        sublistFilespane2.add(start,BorderLayout.EAST);

        listFilespanel.add(sublistFilespanel1,BorderLayout.WEST);
        listFilespanel.add(sublistFilespane2,BorderLayout.EAST);
        contentPane.add(listFilespanel,BorderLayout.NORTH);

        xData = new CopyOnWriteArrayList<>();
        yData = new CopyOnWriteArrayList<>();
        klastersAndPointList = new CopyOnWriteArrayList<>();
        JButton btnNewButtonExit = new JButton("Exit");
        btnNewButtonExit.addActionListener(e -> System.exit(0));
        panelButtons.add(btnNewButtonExit);




        listFilesbutton.addActionListener(actionEvent -> {
            String file = (String) listFiles.getSelectedItem();
            panelData.removeAll();
            if(xData.size()!=0){
                xData.clear();
                yData.clear();
            }
            try {
                List<String> fileList = Files
                        .lines(Paths.get(ClassLoader.getSystemResource(file).toURI()), StandardCharsets.ISO_8859_1)
                        .map(String::toLowerCase)
                        .collect(Collectors.toList());
                addInfoIntoArrays(fileList);
               // JOptionPane.showMessageDialog(listFilesbutton,"End add info");
                contentPane.add(panelData, BorderLayout.CENTER);
                XChartPanel<XYChart> chartPanel;
                chartPanel = createChart(name);
                panelData.add(chartPanel);
                contentPane.validate();
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }

        });

        start.addActionListener(actionEvent -> {
            int countKlaster = Integer.parseInt(klaster.getText());
                klastersAndPointList.clear();
                createKlaster(countKlaster);
                XChartPanel<XYChart> chartPanel;
                chartPanel = createChartWithKlaster(countKlaster);
                panelData.removeAll();
                panelData.add(chartPanel);
                contentPane.validate();

        }
        );

    }

    private void createKlaster(int countKlaster){
        Random rand = new Random();
        double minX = Collections.min(xData);
        double minY = Collections.min(yData);
        double stepX = (Collections.max(xData) - Collections.min(xData)) ;
        double stepY = (Collections.max(yData) - Collections.min(yData)) ;
        for(int i=0;i<countKlaster;i++){
            Klasters klasters = new Klasters(minX+rand.nextInt((int) (stepX+1)),minY+rand.nextInt((int) (stepY+1)));
            KlastersAndPoint klastersAndPoint = new KlastersAndPoint(klasters,new CopyOnWriteArrayList<>(),new CopyOnWriteArrayList<>());
            klastersAndPointList.add(klastersAndPoint);
        }
        sortPointByKlasters(countKlaster);
    }

    private void sortPointByKlasters(int countKlaster){
        for(int k = 0;k<10000;k++) {
            for(int j=0;j<countKlaster;j++){
                klastersAndPointList.get(j).getXdata().clear();
                klastersAndPointList.get(j).getYdata().clear();
            }
            for (int i = 0; i < xData.size(); i++) {
                double min = Double.MAX_VALUE;
                int indexklaster = -1;
                for (int j = 0; j < countKlaster; j++) {
                    double distance = Math.sqrt(Math.pow(xData.get(i) - klastersAndPointList.get(j).getKlaster().getXpoint(), 2.0)
                            + Math.pow(yData.get(i) - klastersAndPointList.get(j).getKlaster().getYpoint(), 2.0));
                    if (min > distance) {
                        min = distance;
                        indexklaster = j;
                    }
                }
                if (indexklaster >= 0) {
                    klastersAndPointList.get(indexklaster).AddXYdata(xData.get(i), yData.get(i));
                }
            }
            if(resortPointAndRelocationKlaster(countKlaster)<0){
                break;
            };
        }
    }

    private int resortPointAndRelocationKlaster(int countKlaster){
        int work = 0;
            for(int i=0; i<countKlaster;i++){
                if(klastersAndPointList.get(i).getXdata().size()!=0){
                    double xsum= 0.0;
                    double ysum = 0.0;
                    for(int j = 0;j<klastersAndPointList.get(i).getXdata().size();j++){
                        xsum+=klastersAndPointList.get(i).getXdata().get(j);
                        ysum+=klastersAndPointList.get(i).getYdata().get(j);
                    }
                    if(Math.abs(klastersAndPointList.get(i).getKlaster().getXpoint()-(xsum/klastersAndPointList.get(i).getXdata().size()))<0.01 &&
                            Math.abs(klastersAndPointList.get(i).getKlaster().getYpoint()-(ysum/klastersAndPointList.get(i).getXdata().size()))<0.01)
                    {
                        work++;
                    }else{
                        klastersAndPointList.get(i).getKlaster().setXpoint((xsum/klastersAndPointList.get(i).getXdata().size()));
                        klastersAndPointList.get(i).getKlaster().setYpoint((ysum/klastersAndPointList.get(i).getYdata().size()));
                    }
                }else{
                    work++;
                }
            }
            if(work==countKlaster){
                return -1;
            }else{
                return 0;
            }

    }

    private XChartPanel createChart(String name) {
        chart = new XYChartBuilder().xAxisTitle("X").yAxisTitle("Y").build();

        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Scatter);
        //chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNE);
        chart.getStyler().setLegendVisible(false);

        XYSeries series = chart.addSeries(name, xData, yData);
        series.setMarker(SeriesMarkers.CIRCLE);
        series.setMarkerColor(colors.get(11));

        XChartPanel<XYChart> chartPanel = new XChartPanel<>(chart);
        return chartPanel;
    }

    private XChartPanel createChartWithKlaster(int countKlaster){
        chart = new XYChartBuilder().xAxisTitle("X").yAxisTitle("Y").build();

        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Scatter);
        //chart.getStyler().setLegendPosition(Styler.LegendPosition.OutsideE);
        chart.getStyler().setLegendVisible(false);

        for(int i=0;i<countKlaster;i++) {
            if(klastersAndPointList.get(i).getXdata().size()!=0) {
                XYSeries series = chart.addSeries(String.valueOf(i), klastersAndPointList.get(i).getXdata(), klastersAndPointList.get(i).getYdata());
                series.setMarker(SeriesMarkers.CIRCLE);
                series.setMarkerColor(colors.get(i%11));
                series.setLabel("North");
            }
        }
        for(int i=0;i<countKlaster;i++) {
                String points = String.format("%d center of klaster",i+1);
                XYSeries series = chart.addSeries(points, new double[]{klastersAndPointList.get(i).getKlaster().getXpoint()},
                        new double[]{klastersAndPointList.get(i).getKlaster().getYpoint()});
                series.setMarker(SeriesMarkers.CIRCLE);
                series.setMarkerColor(colors.get(11));

        }

        XChartPanel<XYChart> chartPanel = new XChartPanel<>(chart);
        return chartPanel;
    }



}
