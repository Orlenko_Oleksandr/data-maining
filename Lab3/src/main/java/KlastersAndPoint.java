import java.util.List;

public class KlastersAndPoint {
    private Klasters klaster;
    private List<Double> xdata;
    private List<Double> ydata;

    public KlastersAndPoint(Klasters klaster, List<Double> xdata, List<Double> ydata) {
        this.klaster = klaster;
        this.xdata = xdata;
        this.ydata = ydata;
    }

    public Klasters getKlaster() {
        return klaster;
    }

    public void setKlaster(Klasters klaster) {
        this.klaster = klaster;
    }

    public void AddXYdata(Double x,Double y){
        this.xdata.add(x);
        this.ydata.add(y);
    }

    public void clearXYdata(){
        this.xdata.clear();
        this.ydata.clear();
    }


    public List<Double> getXdata() {
        return xdata;
    }

    public void setXdata(List<Double> xdata) {
        this.xdata = xdata;
    }

    public List<Double> getYdata() {
        return ydata;
    }

    public void setYdata(List<Double> ydata) {
        this.ydata = ydata;
    }
}
