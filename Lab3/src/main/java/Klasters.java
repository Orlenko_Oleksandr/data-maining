public class Klasters {
   private double xpoint;
   private double ypoint;

    public Klasters(double xpoint, double ypoint) {
        this.xpoint = xpoint;
        this.ypoint = ypoint;
    }


    public double getXpoint() {
        return xpoint;
    }

    public void setXpoint(double xpoint) {
        this.xpoint = xpoint;
    }

    public double getYpoint() {
        return ypoint;
    }

    public void setYpoint(double ypoint) {
        this.ypoint = ypoint;
    }
}
