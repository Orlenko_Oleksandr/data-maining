import java.util.Objects;

public class Item {
    private String stockCode;
    private String description;
    private long quantity;
    private double unitPrice;

    public Item(String stockCode, String description, long quantity, double unitPrice) {
        this.stockCode = stockCode;
        this.description = description;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return stockCode.equals(item.stockCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockCode);
    }

    @Override
    public String toString() {
        return "Item{" +
                "stockCode='" + stockCode + '\'' +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
