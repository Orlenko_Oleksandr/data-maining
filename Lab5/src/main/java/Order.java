import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Order {

    private String invoice;
    private List<Item> items;
    private LocalDateTime invoiceDate;
    private long customerId;
    private String country;

    public Order(String invoice, List<Item> items, LocalDateTime invoiceDate, long customerId, String country) {
        this.invoice = invoice;
        this.items = items;
        this.invoiceDate = invoiceDate;
        this.customerId = customerId;
        this.country = country;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public LocalDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return customerId == order.customerId &&
                invoice.equals(order.invoice) &&
                Objects.equals(items, order.items) &&
                Objects.equals(invoiceDate, order.invoiceDate) &&
                Objects.equals(country, order.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoice);
    }
}