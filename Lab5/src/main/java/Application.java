import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Application {

    private int chromosomesAmount = 500;

    public static void main(String[] args) {
        try {
            new Application().logic();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logic() throws URISyntaxException, IOException {
        Path path = Paths.get(ClassLoader.getSystemResource("retail.xlsx").toURI());

        XSSFWorkbook workbook = new XSSFWorkbook(Files.newInputStream(path));

        int min = 800;

        Sheet sheet = workbook.getSheetAt(0);
        ArrayList<Order> collect = StreamSupport.stream(sheet.spliterator(), false)
                .skip(1)
                .map(this::createOrder)
                .collect(Collectors.collectingAndThen(Collectors.toMap(Order::getInvoice, Function.identity(), this::merge), list -> new ArrayList<>(list.values())));

        Map<Item, Long> items = collect.stream()
                .flatMap(order -> order.getItems().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                //.filter(value -> value.getValue() > min)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        List<Item> itemsArray = items.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry<Item, Long>::getValue).reversed())
                .limit(100)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        List<List<Item>> genericListItems = itemsArray.parallelStream()
                .map(List::of).collect(Collectors.toList());

        int count = 3;

        Map<List<Item>, Long> tempHashMap = new HashMap<>();


        for (int i = 0; i < count-1; i++) {
            for (int j = 0; j < genericListItems.size(); j++) {
                for (int k = 0; k < itemsArray.size(); k++) {
                    if (!genericListItems.get(j).contains(itemsArray.get(k))) {
                        List<Item> comparableItems = new ArrayList<>();
                        comparableItems.addAll(genericListItems.get(j));
                        comparableItems.add(itemsArray.get(k));

                        long itemCount = collect.parallelStream()
                                .map(Order::getItems)
                                .filter(list -> list.containsAll(comparableItems))
                                .count();
                        tempHashMap.put(comparableItems, itemCount);
                    }
                }
            }
            genericListItems = tempHashMap.entrySet().stream()
                    .sorted(Comparator.comparing(Map.Entry<List<Item>, Long>::getValue).reversed())
                    .limit(100)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            itemsArray = genericListItems.stream().flatMap(Collection::stream).distinct().collect(Collectors.toList());
            tempHashMap.clear();
        }

        Optional<List<Item>> first = genericListItems.stream().findFirst();

        first.get().forEach(System.out::println);

        Map<List<Item>, Long> geneticListItems = new HashMap<>();
        chromosomesAmount = (int) (collect.size()*0.1);
        System.out.println(chromosomesAmount);
        init(count, collect, geneticListItems);

        for (int i = 0; i < 1000 && geneticListItems.size()>5; i++) {
            System.out.println(i+"  "+geneticListItems.size());
            mergeArray(count, collect, geneticListItems);
            mutation(count, collect, geneticListItems, itemsArray);
            System.out.println("tmpMax = "+ geneticListItems.entrySet().stream()
                    .max(Comparator.comparing(Map.Entry<List<Item>, Long>::getValue))
                    .map(Map.Entry::getValue)
                    .get());
        }
        List<Item> items1 = geneticListItems.entrySet().stream()
                .max(Comparator.comparing(Map.Entry<List<Item>, Long>::getValue))
                .map(Map.Entry::getKey)
                .get();
        items1.forEach(System.out::println);

    }

    private void init(int count, List<Order> collect, Map<List<Item>, Long> geneticItems) {
        Random random = new Random();
        collect.forEach(order -> {
            List<Item> collect1 = order.getItems().stream().distinct().collect(Collectors.toList());
            order.setItems(collect1);
        });
        List<Integer> visit = new ArrayList<>();
        for (int i = 0; i <collect.size() ; i++) {
            visit.add(0);
        }
        List<Item> tempItems = new ArrayList<>();
        for (int i = 0; i < chromosomesAmount; i++) {
            int index = random.nextInt(collect.size());
            if(collect.get(index).getItems().size()>=count && visit.get(index)==0) {
                for (int j = 0; j <count ; j++) {
                    int indexGen = random.nextInt(collect.get(index).getItems().size());
                    if(!tempItems.contains(collect.get(index).getItems().get(indexGen))){
                        tempItems.add(collect.get(index).getItems().get(indexGen));
                    }
                    else{
                        j--;
                    }
                }
                List<Item> finalTempItems = tempItems;
                long itemCount = collect.parallelStream()
                        .map(Order::getItems)
                        .filter(list -> list.containsAll(finalTempItems))
                        .count();
                geneticItems.put(finalTempItems, itemCount);
                tempItems = new ArrayList<>();
                visit.set(index,1);
            }else{
                i--;
            }
        }
    }

    private void mergeArray(int count, List<Order> collect, Map<List<Item>, Long> geneticItems) {
        List<List<Item>> tempItems = new ArrayList<>(geneticItems.keySet());
        Map<List<Item>,Long> tmpMap = geneticItems.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry<List<Item>, Long>::getValue).reversed())
                .limit(1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        Random random = new Random();
        for (int i = 0; i <tempItems.size() ; i++) {
            List<List<Item>> tmp1 = new ArrayList<>();

            for (int j = 0; j < 2; j++) {
                int i1 = random.nextInt(tempItems.size());
                int i2 = random.nextInt(tempItems.size());
                if(i1!=i2) {
                    if((j > 0 && tmp1.get(0).containsAll(tempItems.get(i1))||(j>0 && tmp1.get(0).containsAll(tempItems.get(i2))))){
                        j--;
                    }else{
                        if (geneticItems.get(tempItems.get(i1)) > geneticItems.get(tempItems.get(i2))) {
                            tmp1.add(tempItems.get(i1));
                            tmp1.add(new ArrayList<>(tempItems.get(i1)));
                        } else {
                            tmp1.add(tempItems.get(i2));
                            tmp1.add(new ArrayList(tempItems.get(i2)));
                        }
                    }
                }else{
                    j--;
                }

            }
            int countGen = random.nextInt(count-1)+1;
            for (int j = 0; j <countGen ; j++) {
                int positionGen = random.nextInt(count);
                if(!tmp1.get(1).contains(tmp1.get(2).get(positionGen))){
                    tmp1.get(1).set(positionGen,tmp1.get(2).get(positionGen));

                }
                if(!tmp1.get(3).contains(tmp1.get(0).get(positionGen))){
                    tmp1.get(3).set(positionGen,tmp1.get(0).get(positionGen));

                }
            }

            long max=0L;
            int position_max=0;
            for (int j = 0; j <tmp1.size() ; j++) {
                if(j%2==0){
                    if(max < geneticItems.get(tmp1.get(j))){
                        max = geneticItems.get(tmp1.get(j));
                        position_max = j;
                    }
                }else{
                    int finalJ = j;
                    long itemCount = collect.parallelStream()
                            .map(Order::getItems)
                            .filter(list -> list.containsAll(tmp1.get(finalJ)))
                            .count();
                    if(max < itemCount){
                        max = itemCount;
                        position_max = j;
                    }
                }
            }
            if(tmpMap.get(tmp1.get(position_max))==null) {
                tmpMap.put(tmp1.get(position_max), max);
            }else{
                if(tmpMap.get(tmp1.get(1))==null) {
                    long itemCount = collect.parallelStream()
                            .map(Order::getItems)
                            .filter(list -> list.containsAll(tmp1.get(1)))
                            .count();
                    tmpMap.put(tmp1.get(1), itemCount);
                }else{
                    long itemCount = collect.parallelStream()
                            .map(Order::getItems)
                            .filter(list -> list.containsAll(tmp1.get(3)))
                            .count();
                    tmpMap.put(tmp1.get(3),itemCount);
                }
            }
        }
        geneticItems.clear();
        geneticItems.putAll(tmpMap);
    }

    private void mutation(int count, List<Order> collect,  Map<List<Item>, Long> geneticItems, List<Item> items) {
        List<List<Item>> collect2 = geneticItems.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry<List<Item>, Long>::getValue).reversed())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        geneticItems.clear();
        Random random = new Random();
        long itemCount = collect.parallelStream()
                .map(Order::getItems)
                .filter(list -> list.containsAll(collect2.get(0)))
                .count();
        geneticItems.put(collect2.get(0), itemCount);
        for (int i = 1; i < collect2.size(); i++) {
            int range = random.nextInt(1000);
            if (range < 10) {
                int countRandom = random.nextInt(count-1);
                int itemsSizeRandom = random.nextInt(items.size());
                while(collect2.get(i).contains(items.get(itemsSizeRandom))) {
                    itemsSizeRandom = random.nextInt(items.size());
                }
                collect2.get(i).set(countRandom, items.get(itemsSizeRandom));
            }
            int finalI = i;
            itemCount = collect.parallelStream()
                    .map(Order::getItems)
                    .filter(list -> list.containsAll(collect2.get(finalI)))
                    .count();
            geneticItems.put(collect2.get(finalI), itemCount);
        }

    }

    private Order createOrder(Row row) {
        Order order = new Order(
                row.getCell(0).toString(),
                new ArrayList<>(),
                row.getCell(4).getLocalDateTimeCellValue(),
                (long) row.getCell(6).getNumericCellValue(),
                String.valueOf(row.getCell(7)));

        order.getItems().add(new Item(
                String.valueOf(row.getCell(1)),
                String.valueOf(row.getCell(2)),
                (long) row.getCell(3).getNumericCellValue(),
                row.getCell(5).getNumericCellValue()
        ));

        return order;
    }

    private Order merge(Order one, Order two) {
        two.getItems().forEach(item -> one.getItems().add(item));

        return one;
    }
}
