import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

// 1- Entered url
// 2- new Main page created.
// 3- search all links
// 4- create pages by those links and check their links

public class PageFactory {

    private static Map<String, Page> pagesCache = new ConcurrentHashMap<>();

    public static Page createPage(String url) {
        return pagesCache.computeIfAbsent(url, Page::new);
    }

    public static Map<String, Page> getPagesCache() {
        return pagesCache;
    }

    public static class Page implements Comparable{
        private String url;
        private double pageRank = 0.5;

        private List<Page> pageReferences = new ArrayList<>();

        public Page(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public List<Page> getPageReferences() {
            return pageReferences;
        }

        public void setPageReferences(List<Page> pageReferences) {
            this.pageReferences = pageReferences;
        }

        public double getPageRank() {
            return pageRank;
        }

        public void setPageRank(double pageRank) {
            this.pageRank = pageRank;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Page page = (Page) o;
            return url.equals(page.url);
        }

        @Override
        public int hashCode() {
            return Objects.hash(url);
        }

        @Override
        public int compareTo(Object o) {
            return ((Page) o).getUrl().compareTo(url);
        }
    }
}