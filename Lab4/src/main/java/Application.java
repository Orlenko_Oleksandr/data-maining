
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.util.mxCellRenderer;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Application {
    private String url;

    private Map<PageFactory.Page, List<PageFactory.Page>> reversedReferences;
    private Queue<PageFactory.Page> queue = new PriorityQueue<>();

    public static void main(String[] args) {
        new Application();
    }

    public Application() {
        logic();
    }

    private void logic() {
        Scanner scanner = new Scanner(System.in);

        //http://www.smallwebsites.co/portfolio
        //https://www.shearpeace.com/
        //"http://www.doveracing.net/"
        url = scanner.nextLine();
        url = url.trim();
        String result = null;

        PageFactory.Page currentPage = PageFactory.createPage(url);
        queue.add(currentPage);

        while(!queue.isEmpty()) {
            PageFactory.Page page = queue.poll();

            Document doc = null;
            try {
                Connection connect = Jsoup.connect(page.getUrl());
                Connection.Response response = connect.ignoreContentType(true).execute();
                //if(Set.of("text/","application/xml","application/xhtml+xml").stream().noneMatch(response.contentType()::contains)) return;

                doc = response.parse();
            } catch (IOException e) {
                e.printStackTrace();
            }

            doc.select("a").forEach((link) -> {
                String href = link.attr("href");

                if(href.startsWith(".")) {
                    href = href.replaceFirst(".", url);
                }// ./about => domain.com/about

                String[] parts = href.split(url);

                if(parts.length == 2 && parts[1].contains(".") && Set.of(".html",".htm",".php", ".aspx", ".asp", ".jsp").stream().noneMatch(parts[1]::contains)) {
                    return;
                }

                if(href.contains(url) && !href.contains("#")) {
                    PageFactory.Page page1 = PageFactory.createPage(href);
                    if(!page.getPageReferences().contains(page1) && !page1.getUrl().equalsIgnoreCase(page.getUrl())) {
                        if(!queue.contains(page1)) {
                            queue.add(page1);
                        }
                        page.getPageReferences().add(page1);
                    }
                }
            });
        }

        this.reversedReferences = PageFactory.getPagesCache().values().stream()
                .collect(Collectors.toMap(Function.identity(), (page) -> new ArrayList<>()));

        reversedReferences.forEach((page, list) -> list.addAll(PageFactory.getPagesCache().values()
                .stream()
                .filter(cache -> cache.getPageReferences().contains(page))
                .collect(Collectors.toList())));

        double max;
        double eps = 0.01;
        do {
            max = 0;
            for (PageFactory.Page page : PageFactory.getPagesCache().values()) {
                double tmpPageRank = 0.5;
                List<PageFactory.Page> pages = reversedReferences.get(page);
                for (PageFactory.Page page1 : pages) {
                    tmpPageRank += (0.5 * page1.getPageRank()) / page1.getPageReferences().size();
                }

                double abs = Math.abs(tmpPageRank - page.getPageRank());

                if(max < abs) {
                    max = abs;
                }

                page.setPageRank(tmpPageRank);
            }
        } while (max > eps);

        PageFactory.getPagesCache().values()
                .stream()
                .sorted(Comparator.comparing(PageFactory.Page::getPageRank).reversed())
                .limit(10)
                .forEach(page -> System.out.println(String.format("%s : %f", page.getUrl(), page.getPageRank())));
        Graph(new ArrayList<>(PageFactory.getPagesCache().values()));
    }

    public void Graph(List<PageFactory.Page> pages){
        DefaultDirectedGraph<String, EdgeWithoutSubscribe> g =
                new DefaultDirectedGraph<String, EdgeWithoutSubscribe>(EdgeWithoutSubscribe.class);

        pages.forEach(page -> g.addVertex(page.getUrl()));

        pages.forEach(page -> page.getPageReferences().forEach(ref -> g.addEdge(page.getUrl(), ref.getUrl())));


        JGraphXAdapter<String, EdgeWithoutSubscribe> graphAdapter =
                new JGraphXAdapter<String, EdgeWithoutSubscribe>(g);
        mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
        layout.execute(graphAdapter.getDefaultParent());

        BufferedImage image =
                mxCellRenderer.createBufferedImage(graphAdapter, null, 2, Color.WHITE, true, null);
        File imgFile = new File("output/graph.png");
        try {
            ImageIO.write(image, "PNG", imgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
